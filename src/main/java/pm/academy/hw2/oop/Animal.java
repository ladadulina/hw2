package pm.academy.hw2.oop;

public interface Animal {
  String getAnimalType();
  String getName();
  int getWeight();
}
