package pm.academy.hw2.oop;

public abstract class FLying implements Animal {
  private long maxFlyingDistance;

  public long getMaxFlyingDistance() {
    return maxFlyingDistance;
  }

  public void setMaxFlyingDistance(long maxFlyingDistance) {
    this.maxFlyingDistance = maxFlyingDistance;
  }
}
